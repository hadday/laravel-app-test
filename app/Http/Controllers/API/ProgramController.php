<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProgramResource;
use App\Services\ProgramService;
use App\Models\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProgramController extends Controller
{

    public function __construct(ProgramService $service)
    {
        $this->programService = $service;
    }


    public function index()
    {
        $data = $this->programService->index();

        return response()->json([ProgramResource::collection($data), 'Programs fetched.']);
    }


    public function show($id)
    {
        $program = $this->programService->show($id);

        if (is_null($program)) {
            return response()->json('Data not found', 404);
        }

        return response()->json([new ProgramResource($program)]);
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'desc' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }

        $program = $this->programService->store($request->name, $request->desc);

        return response()->json(['Program created successfully.', new ProgramResource($program)]);
    }

    public function update(Request $request, Program $program){

        if (is_null($program)) {
            return response()->json('Data not found', 404);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'desc' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 404);
        }

        $program = $this->programService
             ->update($program,
                    $request->name,
                    $request->desc);

        return response()->json(['Program updated successfully.', new ProgramResource($program)]);

    }

    public function destroy(Program $program)
    {
        $program->delete();

        return response()->json('Program deleted successfully');
    }

}
