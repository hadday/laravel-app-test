<?php

namespace App\Services;

use App\Models\Program;

interface ProgramService
{
    public function index();
    public function show($id);
    public function store($name, $desc);
    public function update(Program $program, $name, $desc);
}
