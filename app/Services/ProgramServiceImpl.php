<?php

namespace App\Services;

use App\Models\Program;

class ProgramServiceImpl implements ProgramService
{

    public function index()
    {
        return Program::latest()->get();
    }


    public function show($id)
    {

        $program = Program::find($id);

        return $program;
    }


    public function store($name, $desc)
    {

        $program = Program::create([
            'name' => $name,
            'desc' => $desc
        ]);

        return $program;
    }


    public function update(Program $program, $name, $desc)
    {

        $program->name = $name;
        $program->desc = $desc;
        $program->save();

        return $program;
    }

}
